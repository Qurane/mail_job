# README

1. rvm get stable
2. rvm install 2.7.0
3. rvm use
4. gem install bundler
5. bundle
6. скопировать `.env.sample` в `.env`
7. docker-compose up db
8. rake db:create db:migrate
9. rspec
10. rake db:seed
11. rails s
