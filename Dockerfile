FROM registry.gitlab.com/qurane/mail_job_docker:latest

ENV \
  APP_HOME="/app"

WORKDIR $APP_HOME

# Установка гемов
COPY Gemfile* ./
RUN bundle install

# Копирование исходников
COPY . .

# Установка ассетов
RUN yarn install --check-files && \
    bundle exec rake assets:precompile
