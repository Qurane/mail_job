DatabaseCleaner.clean_with(:truncation, only: %i[courses])

p('courses...')
50.times do |index|
  course = Course.new
  course.name = "Course #{index + 1}"
  course.save!
end
