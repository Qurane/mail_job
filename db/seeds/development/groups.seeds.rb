DatabaseCleaner.clean_with(:truncation, only: %i[groups])

p('groups...')
courses = Course.all
200.times do |index|
  group = Group.new
  group.course = courses.sample
  additional_hours = (index % 5).zero? ? rand(-10..-5) : rand(10..24)
  additional_minutes = rand(60)
  additional_seconds = rand(60)
  group.start_at = DateTime.current + additional_hours.hours + additional_minutes.minutes + additional_seconds.seconds
  group.save!
end
