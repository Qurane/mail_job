DatabaseCleaner.clean_with(:truncation, only: %i[group_students students])

p('students...')
groups = Group.all
400.times do |index|
  student = Student.new
  student.groups = groups.shuffle[0...rand(3)]
  student.email = "example#{index + 1}@example.com"
  student.save!
end
