class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.references :course, null: false
      t.datetime :start_at, null: false
      
      t.timestamps

      t.index %i[course_id start_at], unique: true
    end
  end
end
