class CreateGroupStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :group_students do |t|
      t.references :group, null: false
      t.references :student, null: false

      t.index %i[group_id student_id], unique: true
    end
  end
end
