# frozen_string_literal: true

require_relative 'boot'

require 'active_record/railtie'
# require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_view/railtie'
# require 'action_mailer/railtie'
# require 'active_job/railtie'
# require 'action_cable/engine'
# require 'action_mailbox/engine'
require 'action_text/engine'
# require 'rails/test_unit/railtie'
require 'sprockets/railtie'

Bundler.require(*Rails.groups)

module MailJob
  class Application < Rails::Application
    config.load_defaults 6.0

    config.time_zone = 'Moscow'
    config.i18n.available_locales = %i[en ru]
    config.i18n.default_locale = :ru

    config.action_view.embed_authenticity_token_in_remote_forms = true
  end
end
