# frozen_string_literal: true

Faker::Config.locale = 'en' if %w[development test].include?(Rails.env)
