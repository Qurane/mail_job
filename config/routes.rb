# frozen_string_literal: true

Rails.application.routes.draw do
  root to: redirect('/courses')

  ActiveAdmin.routes(self)

  resources :courses, only: %i[index show]

  post :group_member, to: 'group_members#create'
end
