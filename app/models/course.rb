# frozen_string_literal: true

class Course < ApplicationRecord
  has_many :groups, dependent: :destroy

  validates :name, presence: true, uniqueness: true

  accepts_nested_attributes_for :groups, allow_destroy: true
end
