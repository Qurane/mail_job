# frozen_string_literal: true

class Group < ApplicationRecord
  just_define_datetime_picker :start_at

  belongs_to :course

  has_many :group_students, dependent: :destroy
  has_many :students, through: :group_students

  validates :start_at, presence: true, uniqueness: { scope: :course_id }

  default_scope { order(:start_at) }

  scope :active, -> { where('start_at > ?', DateTime.current) }
end
