# frozen_string_literal: true

class GroupStudent < ApplicationRecord
  belongs_to :group
  belongs_to :student
end
