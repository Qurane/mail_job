# frozen_string_literal: true

class Student < ApplicationRecord
  has_many :group_students, dependent: :destroy
  has_many :groups, through: :group_students

  validates :email, presence: true, uniqueness: true
end
