# frozen_string_literal: true

class GroupMembersController < ApplicationController
  def create
    @group_member = GroupMemberForm.new(group_member_params)

    if @group_member.save
      render status: :ok
    else
      render(:create_failed)
    end
  end

  private

  def group_member_params
    @group_member_params ||= params.require(:group_member_form).permit(:group_id, :email)
  end
end
