# frozen_string_literal: true

class CoursesController < ApplicationController
  breadcrumb I18n.t('courses'), :courses_path

  def index
    @grid = CoursesGrid.new(courses_grid_params) do |scope|
      scope.page(params[:page])
    end
  end

  def show
    breadcrumb course.name, course_path(course.id)

    @groups = course.groups.active.includes(:students).order(:start_at)
  end

  private

  def courses_grid_params
    @courses_grid_params ||= params.fetch(:courses_grid, {}).permit!
  end

  def course
    @course ||= Course.find(params[:id])
  end
end
