# frozen_string_literal: true

class ApplicationController < ActionController::Base
  http_basic_authenticate_with name: ENV['SELF_USERNAME'], password: ENV['SELF_PASSWORD'], if: :admin_controller?

  def admin_controller?
    self.class < ActiveAdmin::BaseController
  end
end
