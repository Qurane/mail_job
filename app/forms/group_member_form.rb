# frozen_string_literal: true

class GroupMemberForm
  include ActiveModel::Model

  ATTRS = %i[group_id email].freeze

  validates :group_id, presence: true
  validates :email, presence: true

  attr_accessor(*ATTRS)

  def initialize(params = {})
    ATTRS.each do |key|
      send("#{key}=", params[key])
    end
  end

  def save
    return false unless valid?

    GroupStudent.transaction do
      GroupStudent.create(
        group: group,
        student: student
      )
    end
  rescue ActiveRecord::RecordNotUnique
    errors.add(:base, I18n.t('already_assigned'))
    false
  end

  def group
    @group ||= Group.find(group_id)
  end

  def student
    @student ||= begin
      Student.find_or_create_by(email: email)
    end
  end
end
