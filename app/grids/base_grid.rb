# frozen_string_literal: true

class BaseGrid
  include Datagrid

  self.default_column_options = {
    # Uncomment to disable the default order
    # order: false,
    # Uncomment to make all columns HTML by default
    # html: true,
  }
  # Enable forbidden attributes protection
  # self.forbidden_attributes_protection = true

  def self.datetime_column(name, text_if_nil, *args)
    column(name, *args) do |model|
      format(block_given? ? yield(model) : model.send(name)) do |date|
        if date.nil?
          text_if_nil
        else
          date.in_time_zone(Time.zone.name).strftime('%d.%m.%Y %H:%M')
        end
      end
    end
  end
end
