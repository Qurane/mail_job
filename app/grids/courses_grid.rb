# frozen_string_literal: true

class CoursesGrid < BaseGrid
  scope do
    Course.includes(groups: :students)
          .joins("left join groups on groups.course_id = courses.id and groups.start_at > '#{DateTime.current}'")
          .select('courses.id, courses.name, min(groups.start_at) over (partition by name) as start_at')
          .distinct
  end

  column(:name, html: true) do |course|
    link_to(course.name, course_path(course), test: "course_#{course.id}_start_at_#{course[:start_at]&.utc || 'none'}")
  end
  datetime_column(
    :start_at,
    I18n.t('course.has_no_active_groups'),
    order: 'start_at'
  ) do |course|
    course[:start_at]
  end
  column(:students_count, html: true) do |course|
    count = if course[:start_at].present?
              course.groups.find { |group| group.start_at == course[:start_at] }.students.length
            else
              '-'
            end
    # rubocop:disable Layout/LineLength
    "<span test=\"course_#{course.id}_students_count_#{course[:start_at].present? ? count : 'none'}\">#{count}</span>".html_safe
    # rubocop:enable Layout/LineLength
  end
end
