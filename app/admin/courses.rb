# frozen_string_literal: true

ActiveAdmin.register Course do
  permit_params :name,
                groups_attributes: %i[id start_at_date start_at_time_hour start_at_time_minute _destroy]

  form do |f|
    f.inputs do
      f.input :name
    end
    f.inputs do
      f.has_many :groups, allow_destroy: true do |t|
        t.input :start_at, as: :just_datetime_picker
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
  end

  filter :name

  show do
    panel 'Groups' do
      table_for course.groups do
        column :start_at
        column :students_count do |group|
          group.students.count
        end
      end
    end
  end
  sidebar 'Course', only: %i[show] do
    attributes_table_for course do
      row :name
    end
  end
end
