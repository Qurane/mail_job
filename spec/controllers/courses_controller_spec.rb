# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CoursesController, type: :controller do
  render_views

  describe 'GET index' do
    let(:students_count) { 3 }
    let(:nearest_start_at) { DateTime.current + 1.hour }

    before do
      DateTime.stub(:current) { '2020-06-04T00:00:00+00:00'.to_datetime }
    end

    it 'has active course with nearest group start date' do
      course = create_active_course

      get :index

      expect(response.body).to match course_start_at_regex(course, nearest_start_at.utc)
      expect(response.body).to match course_students_count_regex(course, 0)
    end

    it 'has inactive course' do
      course = create_inactive_course

      get :index

      expect(response.body).to match course_start_at_regex(course, 'none')
      expect(response.body).to match course_students_count_regex(course, 'none')
    end

    it 'has active course with nearest group start date and students' do
      course = create_active_course_with_students

      get :index

      expect(response.body).to match course_start_at_regex(course, nearest_start_at.utc)
      expect(response.body).to match course_students_count_regex(course, students_count)
    end
  end

  def course_start_at_regex(course, start_at)
    /test="course_#{course.id}_start_at_#{start_at}"/
  end

  def course_students_count_regex(course, count)
    /test="course_#{course.id}_students_count_#{count}"/
  end

  def create_active_course
    course = create(:course)
    create(:group, course: course, start_at: DateTime.current - 1.minute)
    create(:group, course: course, start_at: DateTime.current + 2.hour)
    create(:group, course: course, start_at: nearest_start_at)
    course
  end

  def create_inactive_course
    course = create(:course)
    create(:group, course: course, start_at: DateTime.current - 1.minute)
    course
  end

  def create_course_without_groups
    create(:course)
  end

  def create_active_course_with_students
    course = create(:course)
    students = create_list(:student, students_count)
    create(:group, course: course, start_at: DateTime.current - 1.minute, students: students)
    create(:group, course: course, start_at: DateTime.current + 2.hour, students: students)
    create(:group, course: course, start_at: nearest_start_at, students: students)
    course
  end
end
