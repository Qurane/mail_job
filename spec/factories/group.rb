# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :group do
    course
    start_at { Faker::Time.unique.between(from: 2.days.ago, to: Date.today + 2.days).to_datetime }
  end
end
