# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :student do
    email { Faker::Internet.unique.email }
  end
end
